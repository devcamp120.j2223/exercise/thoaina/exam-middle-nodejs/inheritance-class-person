import { HocSinh } from "./hocSinh.js";
class SinhVien extends HocSinh {
    constructor(hoTen, ngaySinh, queQuan, tenTruong, lop, sdt, chuyenNganh, mssv) {
        super(hoTen, ngaySinh, queQuan, tenTruong, lop, sdt);
        this.chuyenNganh = chuyenNganh;
        this.mssv = mssv;
    }
}

export { SinhVien };