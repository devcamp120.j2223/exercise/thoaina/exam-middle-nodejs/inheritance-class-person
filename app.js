import { ConNguoi } from "./conNguoi.js";
import { HocSinh } from "./hocSinh.js";
import { SinhVien } from "./sinhVien.js";
import { CongNhan } from "./congNhan.js";

let conNguoi1 = new ConNguoi("Nguyễn Thoại", "15/4", "Đồng Tháp");
console.log(conNguoi1);

let hocSinh1 = new HocSinh("Nguyễn Thoại", "15/4", "Đồng Tháp", "THPT Chuyên Nguyễn Quang Diêu", "12", "0909090909");
console.log(hocSinh1);

let sinhVien1 = new SinhVien("Nguyễn Thoại", "15/4", "Đồng Tháp", "FPT", "SE16", "0909090909", "Kỹ Thuật Phần Mềm", "SE160099");
console.log(sinhVien1);

let congNhan1 = new CongNhan("Nguyễn Thoại", "15/4", "Đồng Tháp", "Lập trình viên", "TPHCM", 30000000);
console.log(congNhan1);