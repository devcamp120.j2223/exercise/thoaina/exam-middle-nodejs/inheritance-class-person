import { ConNguoi } from "./conNguoi.js";
class HocSinh extends ConNguoi {
    constructor(hoTen, ngaySinh, queQuan, tenTruong, lop, sdt) {
        super(hoTen, ngaySinh, queQuan);
        this.tenTruong = tenTruong;
        this.lop = lop;
        this.sdt = sdt;
    }
}

export { HocSinh };